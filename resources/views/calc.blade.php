<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Tinta</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        h1 {
            text-align: center;
            margin-bottom: 30px;
        }

        .container {
            max-width: 1000px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .form-group {
            margin-bottom: 20px;
            flex: 1;
        }

        .form-group label {
            display: block;
            font-weight: bold;
            margin-bottom: 5px;
        }

        .form-group input[type="number"] {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .form-group .checkbox-label {
            font-weight: normal;
            display: inline-block;
            margin-left: 10px;
        }

        .btn-submit {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 4px;
            cursor: pointer;
        }

        .btn-submit:hover {
            background-color: #45a049;
        }

        .row{
            display: flex;
            width: 100%;
            align-items: flex-start;
            justify-content: space-evenly;
            gap:20px;
        }
        .result{
            flex: 1;
        }

        .resultTotal{
            text-align: center;
            font-size: 20px;
            margin-bottom: 20px;
        }
        .error{
            flex:1;
            font-size: 15px;
            color:red;
            font-weight: 600;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Calculadora de Tinta</h1>
        <p class="resultTotal" id="resultadoTotal"></p>
        <p class="resultTotal" id="resultadoTotalTinta"></p>
        <p class="resultTotal" id="resultadoTotalLatas"></p>

        <form id="paintForm">
            <div class="row">
                <div id="parede1" class="form-group">
                    <h2>Parede 1</h2>
                   
                    <label for="largura1">Largura (em metros):</label>
                    <input type="number" step="0.01" name="largura1" id="largura1" required>
                    

                    <label for="altura1">Altura (em metros):</label>
                    <input type="number" step="0.01" name="altura1" id="altura1" required>

                    <label for="qtd_janelas">Quantidade de Janelas:</label>
                    <input type="number" step="0.01" name="qtd_janelas1" value="0" id="qtd_janelas1" required>

                    <label for="qtd_portas">Quantidade de Portas:</label>
                    <input type="number" step="0.01" name="qtd_portas1" value="0" id="qtd_portas1" required>

                    <p class="error" id="error1"></p>
                    <div class="result" id="result1"></div>
                </div>


            

                <div id="parede2" class="form-group">
                    <h2>Parede 2</h2>
                    
                    <label for="largura2">Largura (em metros):</label>
                    <input type="number" step="0.01" name="largura2" id="largura2" required>

                    <label for="altura2">Altura (em metros):</label>
                    <input type="number" step="0.01" name="altura2" id="altura2" required>

                    <label for="qtd_janelas2">Quantidade de Janelas:</label>
                    <input type="number" step="0.01" name="qtd_janelas2" value="0" id="qtd_janelas2" required>

                    <label for="qtd_portas2">Quantidade de Portas:</label>
                    <input type="number" step="0.01" name="qtd_portas2" value="0" id="qtd_portas2" required>

                    <p class="error" id="error2"></p>
                    <div class="result" id="result2"></div>
                </div>
            </div>

            <div class="row">
                <div id="parede3" class="form-group">
                    <h2>Parede 3</h2>
                    
                    <label for="largura3">Largura (em metros):</label>
                    <input type="number" step="0.01" name="largura3" id="largura3" required>

                    <label for="altura3">Altura (em metros):</label>
                    <input type="number" step="0.01" name="altura3" id="altura3" required>

                    <label for="qtd_janelas3">Quantidade de Janelas:</label>
                    <input type="number" step="0.01" name="qtd_janelas3" value="0" id="qtd_janelas3" required>

                    <label for="qtd_portas3">Quantidade de Portas:</label>
                    <input type="number" step="0.01" name="qtd_portas3" value="0" id="qtd_portas3" required>

                    <p class="error" id="error3"></p>
                    <div class="result" id="result3"></div>
                </div>

                <div id="parede4" class="form-group">
                    <h2>Parede 4</h2>
                    
                    <label for="largura4">Largura (em metros):</label>
                    <input type="number" step="0.01" name="largura4" id="largura4" required>

                    <label for="altura4">Altura (em metros):</label>
                    <input type="number" step="0.01" name="altura4" id="altura4" required>

                    <label for="qtd_janelas4">Quantidade de Janelas:</label>
                    <input type="number" step="0.01" name="qtd_janelas4" value="0" id="qtd_janelas4" required>

                    <label for="qtd_portas4">Quantidade de Portas:</label>
                    <input type="number" step="0.01" name="qtd_portas4" value="0" id="qtd_portas4" required>

                    <p class="error" id="error4"></p>
                    <div class="result" id="result4"></div>
                </div>
            </div>

            <button type="submit" class="btn-submit">Calcular</button>
        </form>
         
</body>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        document.getElementById("paintForm").addEventListener("submit", function(event) {
            event.preventDefault(); 
            calculatePaint();
        });
        async function calculatePaint(){
            var areaTotal = 0;
            var checkError = false
            for(i = 1; i <=4; i++){
                try{
                    const {data} = await axios({
                        method: 'post',
                        url: '/api/calcular',
                        data: {
                            altura: document.getElementById("altura"+i).value,
                            largura: document.getElementById("largura"+i).value,
                            portas: document.getElementById("qtd_portas"+i).value,
                            janelas: document.getElementById("qtd_janelas"+i).value,
                        }
                    });
                    areaTotal+=data.area
                    document.getElementById("error"+i).innerHTML=''
                    
                    document.getElementById("result"+i).innerHTML=`
                        Área total = ${Math.round(data.area * 100) / 100}m²<br>
                        Quantidade de tinta necessária para está parede = ${Math.round(data.qtd_tinta * 100) / 100} litros.<br>
                        Sugestão de latas para esta parede = ${data.sugestao}`

                }catch(error){
                    checkError = true
                    document.getElementById("result"+i).innerHTML=''
                    document.getElementById("error"+i).innerHTML=error.response.data.error

                }
            }

            if (!checkError){
                try{
                    const {data} = await axios({
                            method: 'post',
                            url: '/api/calcular/total',
                            data: {
                                area: areaTotal
                            }
                    });
                    limpar_campos()
                    document.getElementById("resultadoTotal").innerHTML=`Área total das quatro paredes: ${data.area_total}m².`
                    document.getElementById("resultadoTotalTinta").innerHTML=`Quantidade de tinta necessária para pintar: ${data.qtd_tinta} litros.`
                    document.getElementById("resultadoTotalLatas").innerHTML=`Sugestão de latas: ${data.sugestao}.`
                    
                }catch(error){
                    limpar_campos()
                    document.getElementById("resultadoTotal").innerHTML=error.response.data.error
                }
            }else{
                limpar_campos()
            }         
        }

        function limpar_campos(){
            document.getElementById("resultadoTotal").innerHTML=''
            document.getElementById("resultadoTotalTinta").innerHTML=''
            document.getElementById("resultadoTotalLatas").innerHTML=''
            return
        }
    </script>

</html>