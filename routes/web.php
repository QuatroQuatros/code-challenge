<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CalculoController;

Route::get('/', function () {
    return view('calc');
});

Route::post('/calcular', [CalculoController::class, 'calcular']);
