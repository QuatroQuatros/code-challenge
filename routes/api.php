<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CalculoController;


Route::post('/calcular', [CalculoController::class, 'calcular']);
Route::post('/calcular/total', [CalculoController::class, 'calcularTotal']);
