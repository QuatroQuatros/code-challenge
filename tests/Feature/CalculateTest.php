<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Services\CalculoService;

class CalculateTest extends TestCase
{


    public function test_example(): void
    {
        $service = new CalculoService;

        $response =$service->calcularTotalTinta(4);

        $this->assertEquals([0.5,0.5], $response);

        $response =$service->calcularTotalTinta(6);

        $this->assertEquals([0.5,0.5,0.5], $response);

        $response =$service->calcularTotalTinta(20);

        $this->assertEquals([3.6,0.5], $response);

        $response =$service->calcularTotalTinta(32);

        $this->assertEquals([3.6,2.5,0.5], $response);

        $response =$service->calcularTotalTinta(140);

        $this->assertEquals([18,3.6,3.6,2.5,0.5], $response);
    }

    public function test_FuzzingCalcularTotal()
    {
        $service = new CalculoService;
        $numIterations = 100; // Número de iterações do teste de fuzzing

        for ($i = 0; $i < $numIterations; $i++) {
            $area = mt_rand(0, 100) / 10; // Gera uma área aleatória entre 0 e 10 com uma casa decimal

            // Executa o cálculo da quantidade de tinta e obtém as latas sugeridas
            $latasSugeridas = $service->calcularTotalTinta($area);

            // Verifica se as latas sugeridas são válidas
            $this->assertValidLatas($latasSugeridas, $area);
        }
    }

    private function assertValidLatas($latas, $area)
    {
        $totalArea = array_sum($latas) * 5; // Calcula a área total coberta pelas latas sugeridas

        // Verifica se a área total coberta pelas latas é maior ou igual à área informada
        $this->assertGreaterThanOrEqual($area, $totalArea);
    }
}
