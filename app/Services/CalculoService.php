<?php


namespace App\Services;


class CalculoService{


    public function __construct(){
        $this->latas = [18, 3.6, 2.5, 0.5];
        $this->area_porta =  0.8 * 1.9;
        $this->area_janela = 2 * 1.2;
    }

    // public function calcular(float $altura, float $largura, int $portas=0, int $janelas=0):mixed
    // {
    //     $area = $this->calcularArea
    // }

    public function calcularArea($altura, $largura, $portas=0, $janelas=0):array
    {
        $area_parede = $altura * $largura;

        //algumas validações para a área
        if($area_parede < 1){
            return [
                'success' => false,
                'message' =>   'Área da parede não pode ser menor do que 1 metro.'
            ];
          

        }
        if($area_parede > 50){
            return [
                'success' => false,
                'message' => 'Área da parede não pode ser maior do que 50 metros.'
            ];
        }

        //verificar se a parede tem portas. Caso ela tenha, valida se a altura está dentro da regra.
        if($portas !=0){
            if($altura < 0.3 + 1.9){
                return [
                    'success' => false,
                    'message' => 'Altura informada deve ser de no minímo ' . 0.3 + 1.9 . ' metros.'
                ];     
            }
        }
    
        $area_portas_janelas = $portas * $this->area_porta + $janelas * $this->area_janela;

        //verificar se a área das portas e janelas é maior que 50% da área da parede
        if($area_portas_janelas > $area_parede / 2){
            return [
                'success' => false,
                'message' => 'Área de portas e janelas inválida!'
            ];
        }

        //Se não
        $area_total = $area_parede - $area_portas_janelas;

        return [
            'success' => true,
            'area' => $area_total
        ];
    }

    public function calcularTotalTinta($area)
    {
        $qtd_tinta = $area/5;
        $qtd_latas = [];
        $area_restante = $qtd_tinta;
        // dd($area_restante);

        foreach ($this->latas as $lata ) {
            while ($area_restante >= $lata && $area_restante != 0) {
                if($area_restante >= $lata){
                    array_push($qtd_latas, $lata);
                    $area_restante -= $lata;
                }
            }
               
        }
        // Adiciona uma lata de 0.5 se ainda houver área restante
        if ($area_restante > 0 && $area_restante < 0.5) {
            array_push($qtd_latas, 0.5);
            $area_restante = 0;
        }
        return $qtd_latas;

    }

}