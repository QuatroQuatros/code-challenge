<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class HeightRule implements ValidationRule
{
   
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if($value < 1){
            $fail('Altura informada não pode ser menor do que 1 metro.');
        }
        if($value > 50){
            $fail('Altura informada não pode ser maior do que 50 metros.');
        }
    }

}
