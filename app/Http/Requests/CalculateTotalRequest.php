<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculateTotalRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }
    public function rules(): array
    {
        return [
            'area' => 'required|numeric'
        ];
    }

    public function messages(): array
    {
        return [
            'area.required' => 'A área é obrigatória.',
           
        ];
    }
}
