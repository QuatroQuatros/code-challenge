<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\HeightRule;
use App\Rules\WidthRule;

class CalculateRequest extends FormRequest
{
   
    public function authorize(): bool
    {
        return true;
    }

  
    public function rules(): array
    {
        return [
            'altura' =>  ['required', 'numeric', new HeightRule],
            'largura' => ['required', 'numeric', new WidthRule],
            'portas' => 'integer',
            'janelas' => 'integer',
        ];
    }

    public function messages(): array
    {
        return [
            'altura.required' => 'A altura é obrigatória.',
            'largura.required' => 'A largura é obrigatória.'
        ];
    }
}
