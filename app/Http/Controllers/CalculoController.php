<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CalculateRequest;
use App\Http\Requests\CalculateTotalRequest;

use App\Services\CalculoService;

class CalculoController extends Controller
{
    public function __construct(CalculoService $service){
        $this->service = $service;
    }

    public function calcular(CalculateRequest $request){
        try{
            $response = $this->service->calcularArea($request->altura, $request->largura, $request->portas, $request->janelas);
            if($response['success']){
                $area_total = $response['area'];
                $qtd_latas= $this->service->calcularTotalTinta($area_total);

                return response()->json([
                    'area' => $area_total,
                    'qtd_tinta' => $area_total/5,
                    'sugestao' => $qtd_latas,
                ],200);
            }
            //erro na validação dos dadods
            return response()->json([
                'error' => $response['message']
            ], 422);
           
        }catch(Exception $e){
            return response()->json([
                'error' => $e
            ], 500);
        }
    }

    public function calcularTotal(CalculateTotalRequest $request){
        try{
            $qtd_latas= $this->service->calcularTotalTinta($request->area);

            return response()->json([
                'area_total' => $request->area,
                'qtd_tinta'  => $request->area/5,
                'sugestao'   => $qtd_latas
            ], 200);
        }catch(Exception $e){
            return response()->json([
                'error' => $e
            ], 500);
        }
        
    }

    
}
