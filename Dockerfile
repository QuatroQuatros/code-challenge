FROM php:8.1-apache

# Configure environment variables
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
ENV LOG_CHANNEL=stderr

RUN apt-get update \
    && apt-get install -y --fix-missing \
        wget \
        vim \
        unzip \
        zip \
        net-tools \
        git \
        curl 


RUN apt-get -y install --fix-missing \ 
    dialog \
    gnupg \ 
    openssl \
    ca-certificates \
    lsb-release \ 
    libcurl4 \
    libcurl4-openssl-dev \
    zlib1g-dev \
    libzip-dev \
    libbz2-dev \
    locales \
    libmcrypt-dev \
    libicu-dev \
    libonig-dev \
    libxml2-dev \
    libicu-dev \
    libpq-dev \
    libgmp-dev \
    libsodium-dev \
    libpng-dev

RUN docker-php-ext-configure \
    mysqli

RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    bz2 \
    sodium \
    gd \
    mbstring \
    intl \
    iconv \
    bcmath \ 
    opcache \
    calendar \
    fileinfo \
    zip

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf \ 
    && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf \ 
    && a2enmod rewrite headers



# COPY . /var/www/html
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN  composer install --no-dev

# Configure the permissions of directories
RUN chown -R www-data:www-data /var/www/html \
    && chmod 775 -R /var/www/html/storage/* \
    && chmod 775 /var/www/html/artisan


EXPOSE 80

CMD ["apache2-foreground"]
